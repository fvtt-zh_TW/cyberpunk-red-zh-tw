Hooks.once('init', () => {
    if(typeof Babele !== 'undefined') {
        Babele.get().register({
            module: 'cyberpunk-red-zh-tw',
            lang: 'zh-tw',
            dir: 'compendium'
        });        
    }
});

